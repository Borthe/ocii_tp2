#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "lectorBmp.h"
#include "filtros.h"

/*
 * Funciones en asembler
 */
extern void blendAsm (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);
extern void blendAsmMMX (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);
extern void blendAsmXMM (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);

extern int asmPrint(char * msg, int lenght);

int blend_asm(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsm(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int blend_asmMMX(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsmMMX(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int blend_asmXMM(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsmXMM(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int asm_Print(char * msg)
{
  return asmPrint(msg,strlen(msg));
}

/******************************************************************************/
// Inicia el main

int main (int argc, char* argv[]) {

	BMPDATA bmpData, bmpData2;
	clock_t start, end;
	char opcion[20];
	FILE *out;

	out = fopen("results.csv", "a");
	fprintf(out, "%s  %c %s %c  %s", "Opcion", ',', "Resolucion", ',', "Tiempo\n");
	fclose(out);
	

	/**********************************************/
	loadBmpFile("../img/paisaje52.bmp", &bmpData2);
	
	loadBmpFile("../img/lena52.bmp", &bmpData);
	start = clock();
	multiplyBlending(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendC");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena52.bmp", &bmpData);			
	start = clock();
	blend_asm(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendASM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena52.bmp", &bmpData);
	start = clock();
	blend_asmMMX(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendMMX");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena52.bmp", &bmpData);
	start = clock();
	blend_asmXMM(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendXMM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);
	/********************************************************/

	/**********************************************/
	loadBmpFile("../img/paisaje124.bmp", &bmpData2);
	
	loadBmpFile("../img/lena124.bmp", &bmpData);
	start = clock();
	multiplyBlending(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendC");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena124.bmp", &bmpData);			
	start = clock();
	blend_asm(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendASM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena124.bmp", &bmpData);
	start = clock();
	blend_asmMMX(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendMMX");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena124.bmp", &bmpData);
	start = clock();
	blend_asmXMM(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendXMM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);
	/********************************************************/

	/**********************************************/
	loadBmpFile("../img/paisaje300.bmp", &bmpData2);
	
	loadBmpFile("../img/lena300.bmp", &bmpData);
	start = clock();
	multiplyBlending(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendC");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena300.bmp", &bmpData);			
	start = clock();
	blend_asm(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendASM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena300.bmp", &bmpData);
	start = clock();
	blend_asmMMX(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendMMX");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena300.bmp", &bmpData);
	start = clock();
	blend_asmXMM(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendXMM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);
	/********************************************************/

	/**********************************************/
	loadBmpFile("../img/paisaje512.bmp", &bmpData2);
	
	loadBmpFile("../img/lena512.bmp", &bmpData);
	start = clock();
	multiplyBlending(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendC");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena512.bmp", &bmpData);			
	start = clock();
	blend_asm(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendASM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena512.bmp", &bmpData);
	start = clock();
	blend_asmMMX(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendMMX");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena512.bmp", &bmpData);
	start = clock();
	blend_asmXMM(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendXMM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);
	/********************************************************/

	/**********************************************/
	loadBmpFile("../img/paisaje1024.bmp", &bmpData2);
	
	loadBmpFile("../img/lena1024.bmp", &bmpData);
	start = clock();
	multiplyBlending(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendC");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena1024.bmp", &bmpData);			
	start = clock();
	blend_asm(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendASM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena1024.bmp", &bmpData);
	start = clock();
	blend_asmMMX(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendMMX");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena1024.bmp", &bmpData);
	start = clock();
	blend_asmXMM(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendXMM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);
	/********************************************************/

	/**********************************************/
	loadBmpFile("../img/paisaje2400.bmp", &bmpData2);
	
	loadBmpFile("../img/lena2400.bmp", &bmpData);
	start = clock();
	multiplyBlending(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendC");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena2400.bmp", &bmpData);			
	start = clock();
	blend_asm(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendASM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena2400.bmp", &bmpData);
	start = clock();
	blend_asmMMX(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendMMX");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);

	loadBmpFile("../img/lena2400.bmp", &bmpData);
	start = clock();
	blend_asmXMM(&bmpData, &bmpData2);
	end = clock();
	strcpy(opcion, "MultiplyBlendXMM");
	out = fopen("results.csv", "a");
	fprintf(out, "%s %c %d %c %f %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (double)(end-start)/CLOCKS_PER_SEC, "\n");
	fclose(out);
	/********************************************************/

	limpiarBmpData(&bmpData);
	limpiarBmpData(&bmpData2);
	
	return 0;
}

