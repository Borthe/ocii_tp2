#include "filtros.h"
#include <stdio.h>
#include <stdlib.h>

// intel simd
#if defined(__SSE2__)
#include <emmintrin.h>
#endif

#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a,b) (((a) > (b)) ? (a) : (b))

FILTRO SEPIA = { 
	.393, .760, .189,
	.349, .686, .168,
	.272, .534, .131
};

/******************************************************************************/

void multiplyBlending (BMPDATA *bmpData, BMPDATA *bmpData2) {
	
	for (int i=0; i<cantPixels(bmpData); i++)
	{
		bmpData->red[i]   = (bmpData->red[i] * bmpData2->red[i])/256;
		bmpData->green[i] = (bmpData->green[i] * bmpData2->green[i])/256;
		bmpData->blue[i]  = (bmpData->blue[i] * bmpData2->blue[i])/256;
	}
}

/******************************************************************************/

void aclarar (BMPDATA *bmpData, int nivel) {
	
	for (int i=0; i<cantPixels(bmpData); i++)
	{
		bmpData->red[i]   = min (bmpData->red[i] + nivel , 255);
		bmpData->green[i] = min (bmpData->green[i] + nivel , 255);
		bmpData->blue[i]  = min (bmpData->blue[i] + nivel , 255);
	}
}

/******************************************************************************/

void negativo(BMPDATA *bmpData){

	for (int i=0; i<cantPixels(bmpData); i++)
	{
		bmpData->red[i]   = min (bmpData->red[i] * -1 , 255);
		bmpData->green[i] = min (bmpData->green[i] * -1 , 255);
		bmpData->blue[i]  = min (bmpData->blue[i] * -1 , 255);	
	}
}

/******************************************************************************/

void median_filter(BMPDATA *bmpData, int size)
{
	if(size % 2 == 1 && size > 1){
		int fila = 0;
		int columna = 0;
		int magnitude = size * size;
		int limitMin = ( size/2 ) - 1;
		int limitMax = size/2;
		for (int pixel=0; pixel<cantPixels(bmpData); pixel++)
		{
			if(fila > limitMin && fila < bmpData->infoHeader.biHeight - limitMax 
			&& columna > limitMin && columna < bmpData->infoHeader.biWidth -limitMax)
			{
					int window[magnitude];
					calcularVentana(pixel, bmpData->infoHeader.biWidth, size, window);

					bmpData->red[pixel] = getMedianColor(bmpData->red, window, magnitude);
					bmpData->green[pixel] = getMedianColor(bmpData->green, window, magnitude);
					bmpData->blue[pixel] = getMedianColor(bmpData->blue, window, magnitude);
			}

			if(columna == bmpData->infoHeader.biWidth -1)
			{
				columna = 0;
				fila++;
				continue;
			}
			else{ columna++; }
		}
	} else { 
		printf("El tamaño de la ventana debe ser un número impar mayor a 1 (ejemplo: 3x3 = 3 , 5x5 = 5...");
	}
}

void calcularVentana(int pixel, LONG width, int size, int window[])
{
	int index = 0;
	int end = size / 2;
	int start = end * (-1);
	for (int fila = start; fila <= end ; fila++)
	{
		for (int columna = start; columna <= end; columna++)
		{
			window[index] = pixel + (fila * width) + columna;
			index++;
		}
	}
}

int getMedianColor(unsigned char *color, int window[], int size)
{
	int pixelColors[size];
	for (int index = 0; index < size; index++)
	{
		int pixel = window[index];
		pixelColors[index] = color[pixel];
	}
	int median = getMedian(pixelColors, size);
	return median;
}

int getMedian(int pixelColors[], int size)
{
	int aux;
	for (int i = 0; i < (size - 1); i++) 
	{
		for (int j = i + 1; j < size; j++)
		{
			if (pixelColors[j] < pixelColors[i])
			{
				aux = pixelColors[j];
				pixelColors[j] = pixelColors[i];
				pixelColors[i] = aux;
			}
		}
	}
	int median = size/2;
	return pixelColors[median];
}

/******************************************************************************/

void filtro (BMPDATA *bmpData, FILTRO filtro) {

	for (int i=0; i<cantPixels(bmpData); i++)
	{
		unsigned char r = bmpData->red[i];
		unsigned char g = bmpData->green[i];
		unsigned char b = bmpData->blue[i];
		bmpData->red[i]   = min (r * filtro.RR + g * filtro.RG + b * filtro.RB, 255);
		bmpData->green[i] = min (r * filtro.GR + g * filtro.GG + b * filtro.GB, 255);
		bmpData->blue[i]  = min (r * filtro.BR + g * filtro.BG + b * filtro.BB, 255);
	}
}

/******************************************************************************/

void blancoYNegro (BMPDATA *bmpData) {
	
	for (int i=0; i<cantPixels(bmpData); i++)
	{
	
		unsigned char y = bmpData->red[i] * 0.11448f + bmpData->green[i] * 0.58661f + bmpData->blue[i] * 0.29891f;
		bmpData->red[i]   = y;
		bmpData->green[i] = y;
		bmpData->blue[i]  = y;
	}	
}

/******************************************************************************/

unsigned char mediana (unsigned char *histo, int imediana) {

	int k, aux=0;
	for (k=0; k<255 && aux<=imediana; k++)
		aux += histo[k];

	return k;
}

/******************************************************************************/

int cantPixels(BMPDATA *bmpData) {

	return bmpData->infoHeader.biWidth * bmpData->infoHeader.biHeight;
}