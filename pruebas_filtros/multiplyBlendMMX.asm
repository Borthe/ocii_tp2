section .data
section .bss
section	.text
    global blendAsmMMX	;must be declared for linker (ld)
blendAsmMMX: ;tell linker entry point
	push ebp ;enter 0,0
	mov ebp,esp ;enter 0,0

	xor ecx, ecx
	xor ebx, ebx
	xor eax, eax
	xor edx, edx
	mov edx, [EBP + 32]
	pxor mm5, mm5 ; mm5 = 0 0 0 0 0 0 0 0

start:

	xor eax, eax ;empty eax
	xor ebx, ebx ;empty abx
	mov eax, [EBP + 8] ;red1 color
	mov ebx, [EBP + 20] ;red2 color
	movd mm1, [eax + ecx] ;value red1 color
	movd mm2, [ebx + ecx] ;value red2 color
	punpcklbw mm1, mm5 ; mm1 values = a, mm5 values = b -> mm1 = b a b a b a b a
	punpcklbw mm2, mm5 ; mm1 values = a, mm5 values = b -> mm2 = b a b a b a b a
	pmullw mm1, mm2 ; multiply
	psrlw mm1, 8 ; each words of mm1 / 256
	packuswb mm1, mm1 ; 0 0 0 0 a a a a
	movd [eax + ecx], mm1


	xor eax, eax ;empty eax
	xor ebx, ebx ;empty ebx
	mov	eax, [EBP + 12] ;green1 color
	mov ebx, [EBP + 24] ;green2 color
	movd mm3, [eax + ecx] ;value green1 color
	movd mm4, [ebx + ecx] ;value green2 color
	punpcklbw mm3, mm5 ; mm3 values = a, mm5 values = b -> mm3 = b a b a b a b a
	punpcklbw mm4, mm5 ; mm4 values = a, mm5 values = b -> mm4 = b a b a b a b a
	pmullw mm3, mm4 ; multiply
	psrlw mm3, 8 ; each words of mm1 / 256
	packuswb mm3, mm3 ; 0 0 0 0 a a a a
	movd [eax + ecx], mm3

	xor eax, eax
	xor ebx, ebx
	mov	eax, [EBP + 16] ;blue1 color
	mov ebx, [EBP + 28] ; blue2 color
	movd mm6, [eax + ecx] ;value blue1 color
	movd mm7, [ebx + ecx] ;value blue2 color
	punpcklbw mm6, mm5 ; mm6 values = a, mm5 values = b -> mm6 = b a b a b a b a
	punpcklbw mm7, mm5 ; mm7 values = a, mm5 values = b -> mm7 = b a b a b a b a
	pmullw mm6, mm7 ; multiply
	psrlw mm6, 8 ; each words of mm6 / 256
	packuswb mm6, mm6 ; 0 0 0 0 a a a a
	movd [eax + ecx], mm6

	add ecx, 4
	cmp ecx, edx
	jl start

	mov ebp,esp ;Reset Stack  (leave)
	pop ebp ;Restore old EBP  (leave)
	ret