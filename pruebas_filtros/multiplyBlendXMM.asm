section .data
section .bss
section	.text
    global blendAsmXMM	;must be declared for linker (ld)
blendAsmXMM: ;tell linker entry point
	push ebp ;enter 0,0
	mov ebp,esp ;enter 0,0

	xor ecx, ecx
	xor ebx, ebx
	xor eax, eax
	xor edx, edx
	mov edx, [EBP + 32]
	pxor xmm5, xmm5 ; xmm5 = 0 0 0 0 0 0 0 0

start:

	mov eax, [EBP + 8] ;red1 color
	mov ebx, [EBP + 20] ;red2 color
	movq xmm1, [eax + ecx] ;value red1 color
	movq xmm2, [ebx + ecx] ;value red2 color
	punpcklbw xmm1, xmm5 ; xmm1 values = a, xmm5 values = b -> xmm1 = b a b a b a b a
	punpcklbw xmm2, xmm5 ; xmm1 values = a, xmm5 values = b -> xmm2 = b a b a b a b a
	pmullw xmm1, xmm2 ; multiply
	psrlw xmm1, 8 ; each words of xmm1 / 256
	packuswb xmm1, xmm1 ; 0 0 0 0 a a a a
	movq [eax + ecx], xmm1


	mov	eax, [EBP + 12] ;green1 color
	mov ebx, [EBP + 24] ;green2 color
	movq xmm3, [eax + ecx] ;value green1 color
	movq xmm4, [ebx + ecx] ;value green2 color
	punpcklbw xmm3, xmm5 ; xmm3 values = a, xmm5 values = b -> xmm3 = b a b a b a b a
	punpcklbw xmm4, xmm5 ; xmm4 values = a, xmm5 values = b -> xmm4 = b a b a b a b a
	pmullw xmm3, xmm4 ; multiply
	psrlw xmm3, 8 ; each words of xmm1 / 256
	packuswb xmm3, xmm3 ; 0 0 0 0 a a a a
	movq [eax + ecx], xmm3

	mov	eax, [EBP + 16] ;blue1 color
	mov ebx, [EBP + 28] ; blue2 color
	movq xmm6, [eax + ecx] ;value blue1 color
	movq xmm7, [ebx + ecx] ;value blue2 color
	punpcklbw xmm6, xmm5 ; xmm6 values = a, xmm5 values = b -> xmm6 = b a b a b a b a
	punpcklbw xmm7, xmm5 ; xmm7 values = a, xmm5 values = b -> xmm7 = b a b a b a b a
	pmullw xmm6, xmm7 ; multiply
	psrlw xmm6, 8 ; each words of xmm6 / 256
	packuswb xmm6, xmm6 ; 0 0 0 0 a a a a
	movq [eax + ecx], xmm6

	add ecx, 8
	cmp ecx, edx
	jl start

	mov ebp,esp ;Reset Stack  (leave)
	pop ebp ;Restore old EBP  (leave)
	ret