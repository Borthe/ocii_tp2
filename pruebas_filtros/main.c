#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "lectorBmp.h"
#include "filtros.h"

/*
 * Funciones en asembler
 */
extern void blendAsm (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);
extern void blendAsmMMX (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);
extern void blendAsmXMM (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);

extern int asmPrint(char * msg, int lenght);

int blend_asm(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsm(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int blend_asmMMX(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsmMMX(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int blend_asmXMM(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsmXMM(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int asm_Print(char * msg)
{
  return asmPrint(msg,strlen(msg));
}

/******************************************************************************/
// Inicia el main

int main (int argc, char* argv[]) {

	BMPDATA bmpData, bmpData2;
	
	loadBmpFile ("lena.bmp", &bmpData);
	blancoYNegro(&bmpData);
	saveBmpFile ("lenaBN.bmp", &bmpData);

	loadBmpFile ("lena.bmp", &bmpData);	
	filtro(&bmpData, SEPIA);
	saveBmpFile ("lenaSepia.bmp", &bmpData);

	loadBmpFile ("lena.bmp", &bmpData);
	aclarar(&bmpData, 50);
	saveBmpFile ("lenaAclarar.bmp", &bmpData);

	loadBmpFile ("lena.bmp", &bmpData);
	negativo(&bmpData);
	saveBmpFile ("lenaNeg.bmp", &bmpData);

	loadBmpFile ("salt_pepper.bmp", &bmpData);
	median_filter(&bmpData, 5);
	saveBmpFile ("spMF.bmp", &bmpData);
					
	loadBmpFile ("paisaje.bmp", &bmpData2);

	loadBmpFile ("lena.bmp", &bmpData);
	multiplyBlending(&bmpData, &bmpData2);
	saveBmpFile ("lenaMBC.bmp", &bmpData);

	loadBmpFile ("lena.bmp", &bmpData);
	blend_asm(&bmpData, &bmpData2);
	saveBmpFile ("lenaMBASM.bmp", &bmpData);

	loadBmpFile ("lena.bmp", &bmpData);
	blend_asmMMX(&bmpData, &bmpData2);
	saveBmpFile ("lenaMBMMX.bmp", &bmpData);

	loadBmpFile ("lena.bmp", &bmpData);
	blend_asmXMM(&bmpData, &bmpData2);
	saveBmpFile ("lenaMBXMM.bmp", &bmpData);
										
	limpiarBmpData(&bmpData2);
	limpiarBmpData(&bmpData);
	
	return 0;
}

