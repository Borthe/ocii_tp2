#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "lectorBmp.h"
#include "filtros.h"

/*
 * Funciones en asembler
 */
extern void blendAsm (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);
extern void blendAsmMMX (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);
extern void blendAsmXMM (char *red1, char *green1, char *blue1, char *red2, char *green2, char *blue2, int pixeles);

extern int asmPrint(char * msg, int lenght);

int blend_asm(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsm(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int blend_asmMMX(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsmMMX(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int blend_asmXMM(BMPDATA *bmpData, BMPDATA *bmpData2){
	if(cantPixels(bmpData) != cantPixels(bmpData2)){
		return 1;
	}
	blendAsmXMM(bmpData->red, bmpData->green, bmpData->blue, bmpData2->red, bmpData2->green, bmpData2->blue, cantPixels(bmpData));
	return 0;
}

int asm_Print(char * msg)
{
  return asmPrint(msg,strlen(msg));
}

void primerPrint()
{
	asm_Print("Organización del Computador 2.\nTrabajo Práctico Nro. 2\nPrograma para procesamiento de imágenes BMP.\n");
	printf("\nIngrese el nombre de la imagen que quiera modificar.\n");
}

void printOpciones()
{
	printf("\nPara filtrar la imagen escriba el numero correspondiente.");
	printf("\n1. Blanco y negro.");
	printf("\n2. Sepia.");
	printf("\n3. Aclarar.");
	printf("\n4. Negativo.");
	printf("\n5. Eliminar ruido.");
	printf("\n6. MultiplyBlending.");
	printf("\n7. Guardar imagen.");
	printf("\n8. Salir.\n");
}

void printOpcionesMultiply()
{
	printf("\nElija el tipo de procesamiento: \nSISD = 0 y 1 \nSIMD = 2 y 3\n");
	printf("\n0. MultiplyBlending with C.");
	printf("\n1. MultiplyBlending with Assembler.");
	printf("\n2. MultiplyBlending with MMX.");
	printf("\n3. MultiplyBlending with XMM.");
	printf("\n4. Salir.\n");
}

/******************************************************************************/
// Inicia el main

int main (int argc, char* argv[]) {

	// Creamos results
	FILE *out = fopen("results.csv", "a");
	fprintf(out, "%s  %c %s %c  %s", "Opcion", ',', "Resolucion", ',', "Tiempo\n");
	fclose(out);
	// Fin del creacion

	BMPDATA bmpData, bmpData2;
	clock_t start, end;
	char str1[40], opcion[20];
	
	primerPrint(); 		// Mensaje de introducción y request de nombre de imagen
	scanf("%s", str1);	// Capturamos el nombre de la imagen ingresado

	if (loadBmpFile (str1, &bmpData) != 0) //Verificamos que pueda ser cargable
	{
		printf ("Error al leer el archivo %s\n\n", str1);
	}
	
	if( access( str1, F_OK ) != -1 ) // Verificamos que sea accedible
	{
		printf("\e[1;1H\e[2J"); // Este print hace el Clear en consola
		printf("La imagen ingresada es %s\n", str1);
		
		int eleccion, saveTime, eleccionMultiply;
		printOpciones(); //Imprimimos las opciones del menú
		do
		{	
			scanf("%d", &eleccion); //Capturamos el valor ingresado

			switch (eleccion) // Ejecutamos la opcion elegida
			{
				case 0:
					printOpciones();
					break;

				case 1: 
					start = clock();
					blancoYNegro(&bmpData);
					end = clock();
					saveTime = 1;
					strcpy(opcion,"BlancoNegro");
					break;

				case 2:
					start = clock();
					filtro(&bmpData, SEPIA);
					end = clock();
					saveTime = 1;
					strcpy(opcion, "Sepia");
					break;

				case 3:
					start = clock();
					aclarar(&bmpData, 50);
					end = clock();
					saveTime = 1;
					strcpy(opcion, "Aclarar");
					break;

				case 4:
					start = clock();
					negativo(&bmpData);
					end = clock();
					saveTime = 1;
					strcpy(opcion, "Negativo");
					break;

				case 5:
					start = clock();
					median_filter(&bmpData, 5);
					end = clock();
					saveTime = 1;
					strcpy(opcion, "Filtro de mediana");
					break;

				case 6:
					do
					{
						printf("\nIngrese una nueva imagen para este filtro.\n");
						scanf("%s", str1);
						
						printOpcionesMultiply();
						scanf("%d", &eleccionMultiply); //Capturamos el valor ingresado

						if (loadBmpFile (str1, &bmpData2) == 0)
						{
							if( access( str1, F_OK ) != -1 )
							{
								switch (eleccionMultiply)
								{
									case 0:
										start = clock();
										multiplyBlending(&bmpData, &bmpData2);
										end = clock();
										saveTime = 1;
										strcpy(opcion, "MultiplyBlendC");
										break;

									case 1:
										start = clock();
										blend_asm(&bmpData, &bmpData2);
										end = clock();
										saveTime = 1;
										strcpy(opcion, "MultiplyBlendASM");
										break;

									case 2:
										start = clock();
										blend_asmMMX(&bmpData, &bmpData2);
										end = clock();
										saveTime = 1;
										strcpy(opcion, "MultiplyBlendMMX");
										break;

									case 3:
										start = clock();
										blend_asmXMM(&bmpData, &bmpData2);
										end = clock();
										saveTime = 1;
										strcpy(opcion, "MultiplyBlendXMM");
										break;

									case 4:
										break;
									
									default:
										printf("Valor ingresado incorrecto, vuelva a intentarlo.");
										break;
								}
								limpiarBmpData(&bmpData2);
							}
							else
							{
								printf ("\nError al leer el archivo %s\n\n", str1);							
							}
						}
					} while (eleccionMultiply != 4 && saveTime != 1);
					printf("\nMultiply aplicado, se regresa al menu principal");
					break;

				case 7:
					if (saveBmpFile ("nuevo.bmp", &bmpData) != 0)
						asm_Print("Error al grabar el archivo!");
					else{
						printf("Imagen guardada correctamente");
						loadBmpFile(str1, &bmpData);
					}
					
					break;
				
				case 8:
					break;

				default: //Valor diferente a los esperados
					printf("Valor ingresado incorrecto, vuelva a intentarlo.");
					break;
			}

			if( saveTime == 1)
			{
				// Guardamos los datos de cada operacion en results
				FILE *out = fopen("results.csv", "a");
				int tiempo = end-start;
				fprintf(out, "%s %c %d %c %Lf %s", opcion, ',', bmpData.infoHeader.biHeight * bmpData.infoHeader.biWidth, ',', (long double)tiempo/CLOCKS_PER_SEC, "\n"); // Opcion, Resolucion, tiempo
				fclose(out);
				// Fin del guardado
				
				// Imprime tiempo de cada filtro
				printf("\nTiempo de proceso: %ld ticks.\n", end-start);
				saveTime = 0;
			}

			// Print para repetir las opciones
			printf("\nPara repetir las opciones presione 0.\n");

		} while(eleccion != 8);
		
		// Libera memoria
		limpiarBmpData(&bmpData);
	}
	else
	{
		printf("\nLa imagen ingresada no es correcta.\n");
	}

	printf("\nHasta luego.\n");
	
	//printDatosResolucion(&bmpData);
	return 0;
}

