#ifndef FILTROS_H
#define FILTROS_H

#include "bmp.h"

typedef struct tagFILTRO {

	float RR, RG, RB;
	float GR, GG, GB;
	float BR, BG, BB;
} FILTRO;

FILTRO SEPIA;


void filtro (BMPDATA*, FILTRO);
void blancoYNegro (BMPDATA*);
int cantPixels (BMPDATA*);
unsigned char mediana (unsigned char*, int);

void aclarar (BMPDATA *bmpData, int nivel);

void negativo(BMPDATA *bmpData);

void median_filter(BMPDATA *bmpData, int size);
void calcularVentana(int pixel, LONG width, int size,  int window[]);
int getMedianColor(unsigned char *color, int window[], int magnitude);
int getMedian(int pixelColors[], int magnitude);

void multiplyBlending (BMPDATA *bmpData, BMPDATA *bmpData2);
#endif
