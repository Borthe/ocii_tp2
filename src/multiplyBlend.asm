section .data
	divisor db 255
section .bss
section	.text
    global blendAsm	;must be declared for linker (ld)
blendAsm: ;tell linker entry point
	push ebp ;enter 0,0
	mov ebp,esp ;enter 0,0
	xor ecx, ecx
	mov ecx, 0

start:

	mov ebx, [EBP + 8] ;red1 color
	mov eax, [ebx + ecx] ;value red1 color
	mov edx, [EBP + 20] ;red2 color
	mul BYTE[edx + ecx] ;value red1 color x value red2 color
	div BYTE[divisor]
	mov [ebx + ecx], al
    
	mov	ebx, [EBP + 12] ;green1 color
	mov eax, [ebx + ecx] ;value green1 color
	mov edx, [EBP + 24] ;green2 color
	mul BYTE[edx + ecx] ;value green1 color x value green2 color
	div BYTE[divisor]
	mov [ebx + ecx], al
    
	mov	ebx, [EBP + 16] ;blue1 color
	mov eax, [ebx + ecx] ;value blue1 color
	mov edx, [EBP + 28] ; blue2 color
	mul BYTE[edx + ecx] ;value blue1 color x value blue2 color
	div BYTE[divisor]
	mov [ebx + ecx], al

	add ecx, 1
	cmp ecx, [EBP + 32]
	jnz start

	mov ebp,esp ;Reset Stack  (leave)
	pop ebp ;Restore old EBP  (leave)
	ret
	