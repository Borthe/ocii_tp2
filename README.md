# Procesamiento de imágenes

Proyecto universitario perteneciente a la materia Organización del Computador II. 
- Implementación de un programa de procesamiento de imágenes.
    - Procesamiento a través del lenguaje C.  
    - Procesamiento a través del lenguaje ASM (Assembler). ( Registros de propósito general - Registros MMX - Registros XMM ).

## Prerrequisitos

  - El programa funciona en SO Linux Ubuntu.
  - Las imágenes que quiera modificar deben estar en formato BMP.

### Instalación y ejecución

Instale Git para Ubuntu.

```sh
$ sudo apt-get install git
```

Descargue el proyecto.

```sh
$ sudo git pull https://gitlab.com/borthe/ocii_tp2.git
```
Entre a la carpeta src del proyecto y de permiso al Shell Script.

```sh
$ cd ocii_tp2/src
$ chmod 744 run.sh
```

Ejecute el Shell Script para comenzar.

```sh
$ ./run.sh
```

### Interactuando con el programa

El programa le pedirá que ingrese el nombre de una imagen con su formato. Si la imagen no se encuentra en la carpeta src deberá ingresarla con su PATH.

```sh
Ingrese el nombre de la imagen que quiera modificar.
> lena.bmp
```

Ejemplo con PATH.

```sh
Ingrese el nombre de la imagen que quiera modificar.
> ~/Imágenes/lena.bmp
```

Si la imagen es correcta se le mostrará un menú similar al siguiente.

| Opción | Operación | Lenguaje |
| :---: | :--- | :---: |
| 1 | Filtro 'Blanco y Negro' | C |
| 2 | Filtro 'Sepia' | C |
| 3 | Filtro 'Aclarar' | C |
| 4 | Filtro 'Negativo' | C |
| 5 | Eliminar ruido | C |
| 6 | Blend 'Multiply' | - |
| 7 | Guardar imagen | - |
| 8 | Salir | - |
| 0 | Repetir las opciones | - |

***Nota: La operación 6 mostrará nuevas opciones para el Multiply Blend.***

La opcion 6 pedirá que ingrese una segunda imagen. Si la imagen no se encuentra en la carpeta src deberá ingresarla con su PATH.

```sh
Ingrese una nueva imagen para este filtro.
> paisaje.bmp
```

Ejemplo con PATH.

```sh
Ingrese el nombre de la imagen que quiera modificar.
> ~/Imágenes/paisaje.bmp
```

***Nota: Esta imágen debe tener el mismo tamaño que la primera imágen.***

Si la imágen es correcta se le mostrará un menú similar al siguiente.

| Opción | Método |
| :---: | :---: |
| 0 | C |
| 1 | ASM - Registros de propósito general |
| 2 |  ASM - Registros MMX |
| 3 |  ASM - Registros XMM |
| 4 | Salir |

Al completar la operación se hará un [mash-up](https://www.thefreedictionary.com/Mash-up) entre ambas imágener ingresadas. 

***Nota: Cada operación modificará una copia de la imagen pero no la guardará a menos que se ingrese con la opción correspondiente.***

### Ejemplo de cada operación.

Originales

Lena

![Lena](https://gitlab.com/Borthe/ocii_tp2/-/raw/master/src/lena.bmp "Lena")

Salt and Pepper

![Salt&pepper](https://gitlab.com/Borthe/ocii_tp2/-/raw/master/src/salt_pepper.bmp "Salt and pepper")

Resultados

 - Blanco y negro
 
![BlancoyNegro](https://i.imgur.com/uxTo3w2.png "Blanco y negro")

 - Sepia
 
![Sepia](https://i.imgur.com/ou3KHSA.png "Sepia")

 - Aclarar
 
![Aclarar](https://imgur.com/MunlRCU.png "Aclarar")

 - Negativo
 
![Negativo](https://imgur.com/J8z8nze.png "Negativo")

 - Eliminar ruido
 
![Sin ruido](https://imgur.com/Dt9OMBN.png "Sin ruido")

 - Multiply
 
![Multiply](https://imgur.com/v21bWDh.png "Multiply")

### Resultado

Cada vez que se ejecuta una operación es persistido en un archivo creado por el programa en la carpeta src, llamado results.csv. Este archivo contiene la operación ejecutada, la resolución de la imagen procesada y el tiempo en segundos del tiempo de ejecución. Utilizamos estos datos para analizar la operación Multiply Blend con respecto al tiempo de ejecución con imágenes de diferentes tamaños en cada lenguaje y método utilizado. 

Podemos observar una notable diferencia entre el uso del lenguaje C y registros de propósitos comunes con respecto de los registros MMX y XMM. Esta diferencia de tiempo se acentúa más a medida que las imágenes aumentan de tamaño.

| Resolución | C | ASM | MMX | XMM |
| :--- | :---: | :---: | :---: | :---: |
| 52x52 | 0.000034 | 0.000039 | 0.000004 | 0.000003 |
| 124x124 | 0.000189 | 0.000231 | 0.00017 | 0.000012 |
| 512x512 | 0.009456 | 0.011565 | 0.00277 | 0.001581 |
| 1024x1024 | 0.038414 | 0.045806 | 0.011265 | 0.006113 |
| 2400x2400 | 0.195657 | 0.250761 | 0.056411 | 0.033903 |

![Grafico](https://i.imgur.com/UPJ2W9h.png "Grafico de casos de prueba")

### Conclusión

Tras la ejecución de las pruebas realizadas podemos concluir que, la ejecución de instrucciones y utilización de registros XMM resulta más óptimo para el tratamiento de imágenes de gran tamaño, al igual que con instrucciones y registros MMX, este segundo presenta un desempeño mínimamente más lento. Sin embargo, si la finalidad fuese el procesamiento de imágenes de tamaño chico / mediano, al no haber demasiada diferencia en cuestión de tiempo de ejecución, quizá no sería conveniente su implementación en ASM con registros / instrucciones XMM, ya que esta puede resultar más dificultosa en comparación al lenguaje C.

Con respecto al lenguaje C y ASM (con registros de propósito general), no se notan diferencias en cuanto al tiempo de ejecución. Incluso compiladores modernos logran procesar imágenes en C más rápido que en ASM. Si debiese elegir entre ambos, la recomendación sería C, ya que es más sencillo de implementar (además, se puede configurar para que utilize registros / instrucciones SIMD) y rápido al ejecutar.

![UNGS](http://www.redbien.edu.ar/upload/img/Logo_UNGS_1.jpg "Universidad Nacional de General Sarmiento")

## Organización del Computador II

### Integrantes

- Bornholdt Martin
- Dutra Matías Joel
- Ortelli Gonzalo

### Profesores

- Alexander Agustin
- Alles Sergio Gastón

#### Primer cuatrimestre, 2020
